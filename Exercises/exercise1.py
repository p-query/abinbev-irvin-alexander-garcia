"""
Ejercicio 1: Escribe una función Python para invertir una cadena dada.
Por ejemplo, si la cadena de entrada es "hello", la salida debería ser "olleh".
"""

def invert_string(string: str) -> str:
    invert = string[::-1]
    return f"""
    received string: {string}
    inverted string: {invert}
    """

# Example
print(invert_string("hello")) # Result: olleh
