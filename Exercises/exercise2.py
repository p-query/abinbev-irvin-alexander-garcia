"""
Ejercicio 2: Mejora la función anterior para comprobar si la cadena
dada es un palíndromo. Un palíndromo es una palabra que se lee igual
al revés que al derecho, por ejemplo, "radar".
"""

def invert_string(string: str) -> str:
    invert = string[::-1]
    is_palindrome = False
    # Check if the string is a palindrome
    if string.casefold() == string.casefold()[::-1]:
        is_palindrome = True

    return f"""
    received string: {string}
    inverted string: {invert}
    is palindrome  : {is_palindrome}
    """

# Example
print(invert_string("Reconocer")) # Palindrome
print(invert_string("hello")) # Not a palindrome
