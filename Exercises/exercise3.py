"""
Ejercicio 3: Escriba una función para contar el número
de subcadenas palindrómicas en una cadena dada. Por ejemplo,
en la cadena "aba", hay tres subcadenas palindrómicas: "a", "b", "aba".
"""


def count_palindromes(string: str) -> int:
    count = 0
    for i in range(len(string)):
        if string[i] == string[len(string) - i - 1]:
            count += 1
    return count

# Example
print(count_palindromes("aba")) # Result: 3 - ("a", "b", "aba")
print(count_palindromes("radar")) # Result: 5 - ("r", "a", "d", "rar", "radar")
