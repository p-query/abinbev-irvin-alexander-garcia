"""
Ejercicio 4: Implementa una estructura de datos de pila
en Python usando listas. Tu pila debe soportar operaciones
push, pop y peek.
"""


class Stack:
    """
    A Python implementation of a Stack data structure.
    """
    def __init__(self):
        """
        Initializes an empty stack.
        """
        self.items = []

    def push(self, item: any):
        """
        Pushes an item onto the stack.
        """
        self.items.append(item)

    def pop(self):
        """
        Removes and returns the most recently pushed item.
        """
        if not self.is_empty():
            return self.items.pop()
        else:
            raise IndexError("Stack is empty")

    def peek(self):
        """
        Returns but does not remove the most recently pushed item.
        """
        if not self.is_empty():
            return self.items[-1]
        else:
            raise IndexError("Stack is empty")

    def is_empty(self):
        return len(self.items) == 0

# Example
#--------------- COMMENTED FOR IMPORT CASES ---------------#
# stack = Stack()
# stack.push(1)
# stack.push(2)
# stack.push(3)
# print("Element on top of stack:", stack.peek())
# print("Element removed from stack:", stack.pop())
# print("Element on top of stack after pop:", stack.peek())
#----------------------------------------------------------#
