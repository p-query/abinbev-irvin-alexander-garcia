"""
Ejercicio 5:  crea una estructura de datos de cola. Implementa
las operaciones enqueue y dequeue usando dos instancias de tu pila.
"""


from exercise4 import Stack

class Queue:
    """
    A Python implementation of a Queue data structure.
    """
    def __init__(self):
        """
        Initializes an empty queue.
        """
        self.input_stack = Stack()
        self.output_stack = Stack()

    def enqueue(self, item: any):
        """
        Enqueues an item onto the queue.
        """
        self.input_stack.push(item)

    def dequeue(self):
        """
        Removes and returns the most recently added item.
        """
        if self.output_stack.is_empty():
            if self.input_stack.is_empty():
                raise IndexError("The queue is empty")
            else:
                while not self.input_stack.is_empty():
                    self.output_stack.push(self.input_stack.pop())
        return self.output_stack.pop()

# Example
queue = Queue()
queue.enqueue(1)
queue.enqueue(2)
queue.enqueue(3)
print("Element removed from queue:", queue.dequeue())
print("Element removed from queue:", queue.dequeue())
print("Element removed from queue:", queue.dequeue())
