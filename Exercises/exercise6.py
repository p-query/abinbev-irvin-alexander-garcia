"""
Ejercicio 6: Escriba una función que utilice su implementación
de pila para comprobar si una cadena de paréntesis (por ejemplo,
'((()))', '()()') está equilibrada. Cada paréntesis de apertura
debe tener un paréntesis de cierre correspondiente.
"""

from exercise4 import Stack


def is_balanced(string: str) -> bool:
    """
    Returns True if the string is balanced, False otherwise.
    """
    stack = Stack()
    for char in string:
        if char == "(":
            stack.push(char)
        elif char == ")":
            if stack.is_empty():
                return False
            else:
                stack.pop()
    return stack.is_empty()

# Example
print(is_balanced("((()))")) # Result: True
print(is_balanced("()()")) # Result: True
print(is_balanced("(()()()")) # Result: False
