"""
Ejercicio 7: Escriba un caso de prueba en Python utilizando
el framework unittest para probar una función add(a, b) que
devuelve la suma de dos números. Incluye una prueba que pase
y otra que falle.
"""

import unittest


def add(a: int, b: int) -> int:
    """
    Returns the sum of a and b.
    """
    try:
        return a + b
    except TypeError:
        raise TypeError("Both arguments must be integers")


class TestAdd(unittest.TestCase):
    """
    Tests the add function.
    """
    def test_add(self):
        """
        Tests the add function.
        """
        self.assertEqual(add(1, 2), 3)

class TestAddFailed(unittest.TestCase):
    """
    Tests the add function with errors.
    """
    def test_add_failed(self):
        """
        Tests the add function with errors.
        """
        self.assertEqual(add(2, 3), 6)

if __name__ == '__main__':
    unittest.main()

# Result

# AssertionError: 5 != 6
# ----------------------------------------------------------------------
# Ran 2 tests in 0.010s

# FAILED (failures=1)
