"""
Ejercicio 8: Suponga que tiene una función fetch_data(api_url)
que recupera datos de una API. Escribe un caso de prueba utilizando
unittest.mock para simular la llamada a la API, asegurándote de que
no realiza una petición HTTP real. Comprueba que la función devuelve
una respuesta predefinida
"""
# Unittest
import unittest
from unittest.mock import patch

# Utils
import requests
import json


DUMMY_DATA = {
    'userId': 1,
    'id': 1,
    'title': 'sunt aut facere repellat provident occaecati excepturi' \
             'optio reprehenderit',
    'body': 'quia et suscipit suscipit recusandae consequuntur expedita' \
            'et cum reprehenderit molestiae ut ut quas totam nostrum' \
            'rerum est autem sunt rem eveniet architecto'
}

def fetch_data(api_url: str) -> dict:
    """
    Returns the data from the API.
    """
    data = requests.get(api_url)
    return json.loads(data.text)


class TestFetchData(unittest.TestCase):
    """
    Tests the fetch_data function.
    """
    @patch("__main__.fetch_data")
    def test_fetch_data(self, mock_fetch_data):
        """
        Tests the fetch_data function.
        """
        mock_fetch_data.return_value = DUMMY_DATA
        self.assertEqual(fetch_data("https://api.example.com/api/v1/post/1"), DUMMY_DATA)


if __name__ == '__main__':
    unittest.main()

# Result
# ----------------------------------------------------------------------
# Ran 1 test in 0.001s

# OK
